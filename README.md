# ele

Electronics library to simplify some calculations and finding the right values.

## Examples

### Resistor divider
Divide 5V in half:
```python
from ele import *
print(vdiv(5, 1e3, 1e3)) # gives 2.5
```

### Find resistor combination
Looking for a resistor divider to get 5V from 12V with two resistors in xkΩ range:
```python
print(find_res(lambda rd, ru: vdiv(12, rd ,ru)-5.0, exp=3))
```