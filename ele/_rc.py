# -*- coding: utf-8 -*-
def invsum(*r):
	for ir in r:
		if float(ir) == 0.:
			return 0.
	return 1 / sum([1./float(x) for x in r])

def norsum(*r):
	return sum(r)
