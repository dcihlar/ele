# -*- coding: utf-8 -*-

def ADC(Uin, Uref=5.0, adc_max=1023):
	adc = int(float(Uin)*adc_max/Uref)
	if adc > adc_max:
		adc = adc_max
	return adc
