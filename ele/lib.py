# -*- coding: utf-8 -*-
from . import util


class Component(object):
	ctype_unit_map = {
		'resistor': u'Ω',
		'capacitor': 'F',
		'inductor': 'H',
		'varistor': 'V',
		'choke': u'Ω',
		'zener': 'V',
	}

	def __init__(self, val, raw_val, pkg, lnno, attrs, ctype):
		if ctype not in self.ctype_unit_map:
			raise ValueError('Invalid component type: ' + ctype)

		attrs.append((val, self.ctype_unit_map[ctype]))
		attrs.append((pkg, ''))

		self.val = val
		self.raw_val = raw_val
		self.pkg = pkg
		self.lnno = lnno
		self.attrs = attrs
		self.ctype = ctype

	def test(self, test_val):
		floatfix = 0.0001
		cmpf = {
			'<': lambda a, b: a <= b,
			'>': lambda a, b: a >= b,
			'=': lambda a, b: a == b,
			'!': lambda a, b: a != b,
		}
		op = '='
		if test_val[0] in cmpf.keys():
			op = test_val[0]
			test_val = test_val[1:]
		try:
			val, unit = util.parse_sci_ex(test_val)
			if not unit:
				raise ValueError
		except ValueError:
			val = test_val
			unit = ''
		for attr_val, attr_unit in self.attrs:
			if attr_unit == unit:
				return cmpf[op](attr_val, val)
		return False

	def __repr__(self):
		return '<%s %s %s>' % (self.ctype, self.pkg, self.raw_val)

	def __float__(self):
		return self.val

	def __add__(self, other):
		return float(self) + float(other)

	def __sub__(self, other):
		return float(self) - float(other)

	def __mul__(self, other):
		return float(self) * float(other)

	def __div__(self, other):
		return float(self) / float(other)

	def __neg__(self):
		return -float(self)

	def __pos__(self):
		return float(self)


class ComponentList(list):
	def find(self, *args):
		lst = self
		for arg in args:
			lst = filter(lambda e: e.test(arg), lst)
		return list(lst)


def load_my_smds():
	def parse_packages(es):
		if set(es) & set(['adj', 'array', 'trim']):
			raise ValueError('unsupported component')
		es = list(x.strip() for x in es)
		orig_es = es
		pkgs = []
		attrs = []
		pkg = 'unknown'
		for e in es:
			try:
				val, unit = util.parse_sci_ex(e)
				is_attr = bool(unit)
			except ValueError:
				is_attr = False

			if is_attr:
				attrs.append((val, unit))
			else:
				if pkg != 'unknown' or attrs:
					pkgs.append((pkg, attrs))
					attrs = []
				pkg = e
		pkgs.append((pkg, attrs))

		return pkgs

	lnno = 0
	fname = '/home/dcihlar/Documents/smd_komponente/smd_komponente.txt'
	group = None
	ctype = None
	smds = {}
	for ln in open(fname, 'rt'):
		lnno += 1
		ln = ln.strip()
		if not ln or ln.startswith('#'):
			continue
		elif ln.endswith(':'):
			group = ln[:-1].lower()
			ctype = group[:-1]
		else:
			e = ln.split()
			try:
				val = util.parse_sci(e[0])
				pkgs = parse_packages(e[1:])
			except ValueError:
				# skip unsupported components
				continue

			for pkg, attrs in pkgs:
				component = Component(val, e[0], pkg, lnno, attrs, ctype)
				if group not in smds:
					smds[group] = ComponentList()
				smds[group].append(component)
	return smds

smds = load_my_smds()
