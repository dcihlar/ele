# -*- coding: utf-8 -*-
import itertools
from inspect import signature as function_signature
from . import _rc


stdres_10 = [10, 12, 15, 18, 22, 27, 33, 39, 47, 56, 68, 82]
stdres_5 = [10, 11, 12, 13, 15, 16, 18, 20, 22, 24, 27, 30, 33, 36, 39, 43, 47, 51, 56, 62, 68, 75, 82, 91]
stdres_1 = [	\
10.0, 14.7, 21.5, 31.6, 46.4, 68.1,	\
10.2, 15.0, 22.1, 32.4, 47.5, 69.8,	\
10.5, 15.4, 22.6, 33.2, 48.7, 71.5,	\
10.7, 15.8, 23.2, 34.0, 49.9, 73.2,	\
11.0, 16.2, 23.7, 34.8, 51.1, 75.0,	\
11.3, 16.5, 24.3, 35.7, 52.3, 76.8,	\
11.5, 16.9, 24.9, 36.5, 53.6, 78.7,	\
11.8, 17.4, 25.5, 37.4, 54.9, 80.6,	\
12.1, 17.8, 26.1, 38.3, 56.2, 82.5,	\
12.4, 18.2, 26.7, 39.2, 57.6, 84.5,	\
12.7, 18.7, 27.4, 40.2, 59.0, 86.6,	\
13.0, 19.1, 28.0, 41.2, 60.4, 88.7,	\
13.3, 19.6, 28.7, 42.2, 61.9, 90.9,	\
13.7, 20.0, 29.4, 43.2, 63.4, 93.1,	\
14.0, 20.5, 30.1, 44.2, 64.9, 95.3,	\
14.3, 21.0, 30.9, 45.2, 66.5, 97.6	\
]


def stdres_exp(e, res = stdres_10):
	if type(e) is tuple or type(e) is list:
		multires = []
		for e1 in e:
			multires += stdres_exp(e1, res)
		return multires
	return [r * 10 ** (e - 1) for r in res]

parallel = _rc.invsum
serial = _rc.norsum

def divider(rd, ru):
	"""Returns resistor-rivider ratio of Rd (down) and Ru (up) resistors"""
	return float(rd) / (ru + rd)

def vdiv(volt, rd, ru):
	"""Calculates resistor divider voltage with input "voltage" over divider
	   Rd (down) and Ru (up)"""
	return volt * divider(rd, ru)

def find_res(formula, available_res = stdres_10, exp = None):
	"""Finds a resistor combination that gives "formula" with a lowest value."""
	if exp is not None:
		available_res = stdres_exp(exp, available_res)

	n_args = len(function_signature(formula).parameters)

	best = None
	best_res = None
	for comb in itertools.product(*([available_res] * n_args)):
		x = abs(formula(*comb))
		if best is None or x < best:
			best = x
			best_res = [comb]
		elif best is not None and x == best:
			best_res.append(comb)
	return best_res
