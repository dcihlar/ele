# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from builtins import str

si_units = [ 'A', 'V', u'Ω', 'F', 'H', 'Hz' ]


def parse_sci_ex(val):
	val = str(val).strip()

	# determine SI unit
	unit = ''
	for test_si_unit in si_units:
		if val.endswith(test_si_unit):
			unit = test_si_unit
			val = val[:-len(unit)]
			break

	# determine modifier
	modif = val[-1]
	if modif == u'μ': modif = 'u'
	modifs = 'pnum kMG'
	if modif in modifs:
		modif = 10 ** ((modifs.index(modif) - 4) * 3)
		val = val[:-1]
	else:
		modif = 1
	return float(val) * modif, unit

def parse_sci(val):
	return parse_sci_ex(val)[0]
