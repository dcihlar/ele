import setuptools

setuptools.setup(
	name='ele',
	version='0.1',
	author='Davor Cihlar',
	author_email='davor@cihlar.biz',
	description='Electronics library',
	url='https://bitbucket.org/dcihlar/ele/',
	packages=setuptools.find_packages()
)
